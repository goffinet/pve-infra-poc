# PVE IaaS proof of concept

Setup Proxmox VM infrastructure using Ansible by just defining VMs in a yaml file using the [proxmox_kvm](http://docs.ansible.com/ansible/latest/proxmox_kvm_module.html) module. These VMs will then be [kickstarted/preseeded](https://morph027.gitlab.io/post/pve-kickseed/) for later usage with Ansible (to obtain their roles).

**You need to adjust all vars (storage, network, ...) to some sane values according to the module options!**

## Prerequisites

### General

* Ansible 2.3 (*proxmox_kvm* module added)
* passwordless SSH access to target PVE node(s)

### Python modules

* proxmoxer
* requests

## Usage

* add your target PVE node(s) to `infra` inventory file
* define your VMs in `vars.yml`
* run `ansible-playbook -i infra run.yml`

### Cloud Init

Proxmox now supports cloud-init images. This thingy here supports cloning from a cloud-init VM (like described [here](https://pve.proxmox.com/wiki/Cloud-Init_Support)), so the `template` in cloud-init descriptions must exist before using it. It also requires an [updated](https://github.com/morph027/ansible/blob/proxmox-kvm-cloud-init-settings/lib/ansible/modules/cloud/misc/proxmox_kvm.py) `proxmox_kvm` module (not merged yet, you can put this file into a folder called `library` to use it).

## Notes

* **besides lab testing, you should use [Ansible Vaults](http://docs.ansible.com/ansible/latest/user_guide/playbooks_vault.html#single-encrypted-variable) for PVE API passwords!**
* There is a `contrib` folder which contains kickstart, preseed, ... stuff which is neccessary to run this magic. In a true Ansible way of life one could also deploy templates of these files per vm-deployment instead of doing dynamic cmdline parsing.
